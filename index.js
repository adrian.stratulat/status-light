const express = require('express')
const Gpio = require('onoff').Gpio
const led = new Gpio(4, 'out')
const app = express()

app.post('/', function (req, res) {
  const status = req.body.status

  if (status === 'on') {
    led.writeSync(1)
  } else if (status === 'off') {
    led.writeSync(0)
  } else {
    res.status(500)
  }

  res.send('LED as a service')
})

app.listen(3000, () => console.log('App listening on port 3000.'))
